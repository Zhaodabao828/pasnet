import numpy as np
import random as rn
import numpy
from keras.layers import *
import keras
from keras.engine.topology import Layer
from keras import initializers
import warnings
import DataRep
from keras import initializers
from keras.models import Model
from keras.optimizers import Adam,RMSprop,Adagrad,Adadelta
from keras.layers import *
from keras.layers.noise import *
from keras.regularizers import l2
from keras.layers.embeddings import Embedding
from keras import backend as K
from keras.initializers import Constant
from attention import *
from utils_baseline import *
seed = 2019
L2_value=0.0001
def Gated_conv1d(seq,kernel_size):
    dim = K.int_shape(seq)[-1]
    h = Conv1D(dim*2, kernel_size, padding='same', kernel_initializer = initializers.lecun_normal(seed = seed),kernel_regularizer=l2(L2_value))(seq)
    def _gate(x):
        s, h = x
        g, h = h[:, :, :dim], h[:, :, dim:]
        g = K.sigmoid(g)
        h = K.selu(h)
        return (1-g) * s + g * h
    seq = Lambda(_gate)([seq, h])
    return seq
def build_PASNet(args, word_index, nb_classes,dropout_rate,filters,embedding_matrix,l_r,ks,units):
    # build the PASNet framework
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    #1
    ini_featuresa0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features)  
    ini_featuresa2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa = keras.layers.add([ini_featuresa0,ini_featuresa1,ini_featuresa2]) 
    g_features1 = Gated_conv1d(ini_featuresa, ks)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features2 = Gated_conv1d(ini_featuresb, ks)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    ini_featuresc0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2)  
    ini_featuresc2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc = keras.layers.add([ini_featuresc0,ini_featuresc1,ini_featuresc2]) 
    g_features3 = Gated_conv1d(ini_featuresc, ks)
    # g_features3 = Dropout(dropout_rate)(g_features3)
    # #4
    # ini_featuresd0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features3) 
    # ini_featuresd1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features3)  
    # ini_featuresd2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features3) 
    # ini_featuresd = keras.layers.add([ini_featuresd0,ini_featuresd1,ini_featuresd2]) 
    # g_features4 = Gated_conv1d(ini_featuresd, ks)
    # #5
    # ini_featurese0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features4) 
    # ini_featurese1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features4)  
    # ini_featurese2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features4) 
    # ini_featurese = keras.layers.add([ini_featurese0,ini_featurese1,ini_featurese2]) 
    # g_features5 = Gated_conv1d(ini_featurese, ks)

    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    # s_features = Attention(8,16)([g_features2, g_features2, g_features2]
    features = keras.layers.concatenate([s_features, g_features3])
    # features = Attention(8,16)([g_features3, g_features3, g_features3])

    all_features = Flatten()(features)
    all_features = Dropout(dropout_rate)(all_features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    PASNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    PASNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    PASNet.summary()
    PASNet.get_config()
    return PASNet


def build_onehotPASNet(args, word_index, nb_classes,dropout_rate,filters,embedding_matrix,l_r,ks,units):
    # build the PASNet framework using the one-hot vectors
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        len(word_index),
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    #1
    ini_featuresa0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features)  
    ini_featuresa2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa = keras.layers.add([ini_featuresa0,ini_featuresa1,ini_featuresa2]) 
    g_features1 = Gated_conv1d(ini_featuresa, ks)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features2 = Gated_conv1d(ini_featuresb, ks)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    ini_featuresc0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2)  
    ini_featuresc2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc = keras.layers.add([ini_featuresc0,ini_featuresc1,ini_featuresc2]) 
    g_features3 = Gated_conv1d(ini_featuresc, ks)

    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    features = keras.layers.concatenate([s_features, g_features3])


    all_features = Flatten()(features)
    all_features = Dropout(dropout_rate)(all_features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    onehotPASNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    onehotPASNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    onehotPASNet.summary()
    onehotPASNet.get_config()
    return onehotPASNet

def build_randPASNet(args, word_index, nb_classes,dropout_rate,filters,l_r,ks,units):
    # build the PASNet framework using the random vectors
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        input_length=args.sequence_length,
                        trainable=args.trainable)

    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    #1
    ini_featuresa0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features)  
    ini_featuresa2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa = keras.layers.add([ini_featuresa0,ini_featuresa1,ini_featuresa2]) 
    g_features1 = Gated_conv1d(ini_featuresa, ks)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features2 = Gated_conv1d(ini_featuresb, ks)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    ini_featuresc0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2)  
    ini_featuresc2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc = keras.layers.add([ini_featuresc0,ini_featuresc1,ini_featuresc2]) 
    g_features3 = Gated_conv1d(ini_featuresc, ks)

    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    features = keras.layers.concatenate([s_features, g_features3])

    all_features = Flatten()(features)
    all_features = Dropout(dropout_rate)(all_features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    randPASNet = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    randPASNet.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    randPASNet.summary()
    randPASNet.get_config()
    return randPASNet


def build_noGCHN(args, word_index, nb_classes,dropout_rate,filters,embedding_matrix,l_r,ks,units):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
   
    s_features = Attention(8,16)([embed_features, embed_features, embed_features])
    # features = Attention(8,16)([g_features3, g_features3, g_features3])
    all_features = Flatten()(s_features)
    all_features = Dropout(dropout_rate)(all_features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    noGCHN = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    noGCHN.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    noGCHN.summary()
    noGCHN.get_config()
    return noGCHN
    
def build_noSelfAttention(args, word_index, nb_classes,dropout_rate,filters,embedding_matrix,l_r,ks,units):
    print('<<<<<<<<<<<<<<<build_noSelfAttention>> GCHN model>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    embed_features = Dropout(dropout_rate)(embed_features)
    #1
    ini_featuresa0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features)  
    ini_featuresa2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(embed_features) 
    ini_featuresa = keras.layers.add([ini_featuresa0,ini_featuresa1,ini_featuresa2]) 
    g_features1 = Gated_conv1d(ini_featuresa, ks)
    g_features1 = Dropout(dropout_rate)(g_features1)
    #2
    ini_featuresb0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1)  
    ini_featuresb2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features1) 
    ini_featuresb = keras.layers.add([ini_featuresb0,ini_featuresb1,ini_featuresb2]) 
    g_features2 = Gated_conv1d(ini_featuresb, ks)
    g_features2 = Dropout(dropout_rate)(g_features2)
    #3
    ini_featuresc0 = Conv1D(filters, ks, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc1 = Conv1D(filters, ks+6, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2)  
    ini_featuresc2 = Conv1D(filters, ks+12, padding='same', activation = 'selu', kernel_initializer = initializers.lecun_normal(seed = seed), kernel_regularizer=l2(L2_value))(g_features2) 
    ini_featuresc = keras.layers.add([ini_featuresc0,ini_featuresc1,ini_featuresc2]) 
    g_features3 = Gated_conv1d(ini_featuresc, ks)
 
    all_features = Flatten()(g_features3)
    all_features = Dropout(dropout_rate)(all_features)
    fc1 = Dense(units, activation='selu',kernel_initializer = initializers.lecun_normal(seed = seed))(all_features)
    output = Dense(nb_classes,activation='softmax')(fc1)
    noSelfAttention = Model(inputs=[input_sequences], outputs=[output])
    optimizer =  Adam(lr=l_r, beta_1=0.9, beta_2=0.999, epsilon=1e-08)
    noSelfAttention.compile(optimizer= optimizer, loss='binary_crossentropy', metrics=['accuracy']) 
    noSelfAttention.summary()
    noSelfAttention.get_config()
    return noSelfAttention


