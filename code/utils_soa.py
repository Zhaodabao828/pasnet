import numpy as np
import random as rn
import numpy
from keras.layers import *
import keras
from keras.engine.topology import Layer
from keras import initializers
import warnings
import DataRep
from keras import initializers
from keras.models import Model
from keras.optimizers import Adam,RMSprop,Adagrad,Adadelta,SGD
from keras.layers.noise import *
from keras.regularizers import l2
from keras.layers.embeddings import Embedding
from keras import backend as K
from keras.initializers import Constant
seed =2019
def build_DeepPolya(args, word_index, nb_classes,embedding_matrix):
    nb_filter1 =16
    nb_filter2 = 64
    filter_length1 = 8
    filter_length2 = 6
    units = 64

    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)
    #1
    conv1 = Conv1D(nb_filter1, filter_length1, padding='valid', activation = 'relu', kernel_initializer = initializers.glorot_normal(seed = seed), subsample_length = 1)(embed_features) 
    conv1 = Dropout(0.4)(conv1)
    #2
    conv2 = Conv1D(nb_filter2, filter_length2, padding='valid', activation = 'relu', kernel_initializer = initializers.glorot_normal(seed = seed), subsample_length = 1)(conv1)
    conv2 = MaxPooling1D(pool_length=2, stride=2)(conv2)
    conv2 = Dropout(0.4)(conv2)
    features = Flatten()(conv2)

    fc1 = Dense(units, activation='relu',kernel_initializer = initializers.glorot_uniform(seed = seed))(features)
    fc1 = Dropout(0.4)(fc1)

    output = Dense(nb_classes, activation='softmax',kernel_initializer = initializers.glorot_uniform(seed = seed))(fc1)
   
    DeepPolya = Model(inputs=[input_sequences], outputs=[output])
    sgd = SGD(lr=0.001, momentum=0.9, decay=1e-5, nesterov=True)
    # optimizer =  'Adadelta'
    DeepPolya.compile(optimizer= sgd, loss='binary_crossentropy', metrics=['accuracy']) 
    DeepPolya.summary()
    DeepPolya.get_config()
    return DeepPolya


def build_DeepPASTA(args, word_index, nb_classes,embedding_matrix):
    print('<<<<<<<<<<<<<<<embedding>>>>>>>>>>>>>')
    embedding_layer = Embedding(len(word_index) + 1,
                        args.n,
                        weights=[embedding_matrix],
                        input_length=args.sequence_length,
                        trainable=args.trainable)
    input_sequences = Input(shape=(args.sequence_length,), dtype='float32')
    embed_features = embedding_layer(input_sequences)

    seqCov = Conv1D(filters=512, kernel_size=8, padding = "valid", activation="relu", strides=1,kernel_initializer = initializers.glorot_uniform(seed = seed))(embed_features)             
    seqPool = MaxPooling1D(pool_size = 3, strides = 3)(seqCov)
    seqDout1 = Dropout(rate = 0.7)(seqPool)
    seqBiLstm = Bidirectional(LSTM(units = 128, return_sequences = True,kernel_initializer=initializers.glorot_uniform(seed = seed)))(seqDout1)
    seqDout2 = Dropout(rate = 0.7)(seqBiLstm)
    seqFlat = Flatten()(seqDout2)
    seqDen2 = Dense(256, kernel_initializer=initializers.glorot_uniform(seed = seed), activation = 'relu')(seqFlat)
    seqDout4 = Dropout(rate = 0.7)(seqDen2)  

    den2 = Dense(128, kernel_initializer = initializers.glorot_uniform(seed = seed), activation = 'relu')(seqDout4)
    dout2 = Dropout(rate = 0.7)(den2)
    den3 = Dense(64, activation = 'relu',kernel_initializer = initializers.glorot_uniform(seed = seed))(dout2)
    output = Dense(nb_classes, activation = 'softmax',kernel_initializer=initializers.glorot_uniform(seed = seed))(den3) 

    DeepPASTA = Model(inputs=[input_sequences], outputs=[output])
  
    DeepPASTA.compile(optimizer= 'nadam', loss='binary_crossentropy', metrics=['accuracy']) 
    DeepPASTA.summary()
    DeepPASTA.get_config()


    return DeepPASTA
