# PASNet.
Polyadenylation is responsible for regulating mRNA
function and translation efficiency. Accurately identifying
polyadenylation signals (PAS) is essential for understanding
genome annotation, gene regulation and function. Current
computational methods mainly rely on human-designed features
for special PAS, and cannot automatically extract underlying
discriminative patterns for identifying different types of PAS
from different organism genomes. Furthermore, identifying PAS
from primary DNA sequences that correspond to polyadenylation
signals of mRNA is a far more challenging task, and there is still
room for further improvement. In this work, we first adopt a
shallow neural network to automatically learn the sequence
embedding with discriminative information from DNA sequences.
Then, we design a robust deep learning framework termed
PASNet, which is an improved gated convolutional highway
network with self-attention mechanism, to automatically extract
underlying patterns of DNA sequences and identify PAS on
different organism data.
1. Gnerating k-mer sequences
The 'Datatransform' is used to generated the k-mer sequences of the genome.
2. Genrerating embedding vectors. 
The pretrained code of embedding vectors: https://github.com/tmikolov/word2vec. 
3. Training and testing models..

