# all genome data of four different organisms
You can download the genome data for polyadenylation signals of four organisms including 
Homo sapiens (human), Mus musculus (mouse), Bos taurus (bovine) and Drosophila melanogaster (fruit fly)
from this link (https://pan.baidu.com/s/1fY7LfJD42szPX_4WVypuXQ;https://zenodo.org/record/1117159#.YIIfDOgzZPY) and then put the file in the current directory.

The 'Datatransform' is used to generated the k-mer sequences of the genome.
