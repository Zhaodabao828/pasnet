
from numpy import *
import numpy as np
import sys
import scipy.io as spio

def Data2Text(posFile,negFile,WordLength,tag):

	WordLength=int(WordLength)
	FileName=tag+'_Data2Text_word%d.txt' %WordLength
	
	#load data from files
	PosSamples = list(open(posFile).readlines())
	PosSamples = [s.strip() for s in PosSamples] #remove \n

	NegSamples = list(open(negFile).readlines())
	NegSamples = [s.strip() for s in NegSamples] #remove \n
	
	print('PosSamples= '+str(len(PosSamples)))
	print('NegSamples= '+str(len(NegSamples)))
		
	#add space between the sequence to convert it into sentence of words
	PosSentences = []
	NegSentences = []
	
	#-1 because of the label at the end of the samples 
	for s in PosSamples:
		x=''
		for i in range(0,len(s)-1):
			#check if it is the last word
			if i==len(s)-1-WordLength:
				x=x+s[i:i+WordLength]
				break
			else:
				x=x+s[i:i+WordLength]+" "
		PosSentences.append(x)

	for s in NegSamples:
		x=''
		for i in range(0,len(s)-1):
			#check if it is the last word
			if i==len(s)-1-WordLength:
				x=x+s[i:i+WordLength]
				break
			else:
				x=x+s[i:i+WordLength]+" "
		NegSentences.append(x)
		
	x='Data/pos_'+FileName
	np.savetxt(x,PosSentences,fmt='%s')
	y='Data/neg_'+FileName
	np.savetxt(y,NegSentences,fmt='%s')	
	
	#Split by words
	data = PosSentences + NegSentences
	
	z='Data/data_'+FileName
	np.save(z,data)
	
	#Generate labels
	PosLabels = [[0, 1] for _ in PosSentences]
	NegLabels = [[1, 0] for _ in NegSentences]
	
	labels = np.concatenate([PosLabels, NegLabels])

	m='Data/labels_'+FileName
	np.save(m,labels)

	
	return [x,len(PosSentences),y,len(NegSentences),data,labels]
	
